Imports Microsoft.Office.Interop

Module ModMain

    Public Const connectionstring As String
    Sub Main()

        Dim oxl As New Excel.Application
        Try
            Dim oWB As Excel.Workbook
            Dim oSheet As Excel.Worksheet
            Dim oRange As Excel.Range = Nothing
            oWB = oxl.Workbooks.Open("my_file_path")
            oSheet = CType(oWB.Sheets("Sheet1"), Microsoft.Office.Interop.Excel.Worksheet)


            oSheet = Nothing
            oRange = Nothing
            oWB.Close()
            oWB = Nothing
        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally 'Close the Excel process so it does not continue to run
            oxl.Quit()
        End Try
    End Sub

End Module
